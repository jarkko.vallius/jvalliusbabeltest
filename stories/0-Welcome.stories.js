import React from 'react';
import { storiesOf } from '@storybook/react';
import {Hello, Dnd} from '../src'
// import {World} from '../dist/bundle'


storiesOf('Hello', module)
    .addParameters({ component: Hello })
    .add('example', () => <Hello />)
    .add('custom title', () => { 
        const custom = "moi"
        return (
            <Hello title={custom} />
        )
    })
storiesOf('Dnd', module)
.addParameters({ component: Dnd })
.add('example', () => {

    const schema = {
        elements : [
            {
                type : "grid",
                elementProps : {
                    direction : "column"
                },
                elements : [
                    {
                        type : "text",
                        name : "hello"
                    },
                    {
                        type : "text",
                        name : "world"
                    },
                ]
            },
        ]
    }

    return <Dnd formSchema={schema} />
})
