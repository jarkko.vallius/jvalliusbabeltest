import React from 'react'

const World = (props) => {

    const test = () => {
        return JSON.stringify("foobar".includes("foo"))
    }
    
    return (
        <>
        <div>foobar</div>
        {test()}
        </>

    )
}

export default World