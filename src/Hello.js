import React from 'react'
import {Button} from '@material-ui/core'
import PropTypes from 'prop-types';

/**
 * Tämä on hello komponentin description
 * 
 */
const Hello = (props) => {
    const {title} = props
    return (
        <>
            <div>Hello React</div>
            <div>{title}</div>
            <Button variant="contained" color="secondary">nappi</Button>

        </>
    )
}
           
Hello.propTypes = {
    /**
     * Title
     */
    title : PropTypes.string
}

Hello.defaultProps = {
    title : "not_set"
}

export default Hello