import React, {useState} from 'react'
import {Grid, FormControl, TextField} from '@material-ui/core'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
const uuid = require('uuid/v4');

const getItems = count =>
  Array.from({ length: count }, (v, k) => k).map(k => ({
    id: `item-${k}`,
    content: `item ${k}`
  }));

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const add = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    // const [removed] = sourceClone.splice(droppableSource.index, 1);

    let copy = JSON.parse(JSON.stringify(sourceClone[droppableSource.index]))

    let randomId = uuid()
    copy.draggableId = "SCHEMA." + randomId
    copy.name = "name" + randomId

    destClone.splice(droppableDestination.index, 0, copy);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};



const grid = 4

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    // display: 'flex',
    // padding: grid,
    // overflow: 'auto',
  });

// const getItemStyle = (isDragging, draggableStyle) => ({
//     // some basic styles to make the items look a bit nicer
//     userSelect: "none",
//     padding: grid * 2,
//     margin: `0 0 ${grid}px 0`,
  
//     // change background colour if dragging
//     background: isDragging ? "lightgreen" : "grey",
  
//     // styles we need to apply on draggables
//     ...draggableStyle
//   });

  const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    // padding: grid * 2,
    // margin: `0 ${grid}px 0 0`,
  
    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',
    flexGrow : 1,
    // styles we need to apply on draggables
    ...draggableStyle,
  });
const modifySchema = (props) => {
    let {obj, decorator} = props
    if (Array.isArray(obj)) {
        obj.map( e => modifySchema({obj:e, decorator}))
    }

    if (obj.elements != null) {
        modifySchema({obj:obj.elements, decorator})
    }

    obj = decorator(obj)


    return obj

}

const addDndIds = (schema, prefix)=>{
    modifySchema({obj:schema, decorator : data => {
        if (data.type == "grid") {
            data.droppableId = prefix + uuid()

            if (data.elements) {
                data.elements.map(e => e.draggableId = prefix + uuid())
            }
        }
        return data
    }})
}

const Dnd = (props) => {

    const {formSchema} = props


    const [elementsListSchema] = useState(()=>{
        let cpy = {
            elements : [
                {
                    type : "grid",
                    elements : [
                        {
                            type : "text",
                            name : "TEKSTIKENTTÄ"
                        },
                    ]
                    
                }
            ]
        }
        addDndIds(cpy, "ELEMENT.")
        return cpy
    })

    const [schema, setSchema] = useState(()=>{
        let cpy = JSON.parse(JSON.stringify(formSchema))
        addDndIds(cpy, "SCHEMA.")
        return cpy
    })





    // TODO
    const onDragEnd = (result) => {

        const { source, destination } = result;

        console.log(result)

        // dropped outside the list
        if (!destination) {
            return;
        }

        let from = source.droppableId.split(".")[0]
        let to = destination.droppableId.split(".")[0]
        let sourceObj = null
        let destinationObj = null
        let schemaCopy = JSON.parse(JSON.stringify(schema))

        if (from == "SCHEMA" && to == "SCHEMA") {

            modifySchema({obj:schemaCopy, decorator: data => {
                // console.log("decoratorDataName", data.name)
                if (data.droppableId == destination.droppableId) {
                    destinationObj = data
                }
                if (data.droppableId == source.droppableId) {
                    sourceObj = data
                }
                return data
            }})

            // Re-order draggables in same droppableId
            if (source.droppableId === destination.droppableId) {

                console.log("REORDER")
                const newItems = reorder(
                    destinationObj.elements,
                    result.source.index,
                    result.destination.index
                );

                destinationObj.elements = newItems


            } else {
                // Move draggable to another droppable
                const moveResults = move(
                    sourceObj.elements,
                    destinationObj.elements,
                    source,
                    destination
                );
                sourceObj.elements = moveResults[source.droppableId]
                destinationObj.elements = moveResults[destination.droppableId]

            }

            modifySchema({obj:schemaCopy, decorator: data => {
                // console.log("decoratorData", data)
                if (data.droppableId == destination.droppableId) {
                    data.elements = destinationObj.elements
                }
                if (data.droppableId == source.droppableId) {
                    data.elements = sourceObj.elements
                }
                return data
            }})

    
        } else if (from == "ELEMENT" && to == "SCHEMA") {
            modifySchema({obj:elementsListSchema, decorator: data => {
                // console.log("decoratorDataName", data.name)
                if (data.droppableId == source.droppableId) {
                    sourceObj = data
                }
                return data
            }})
            modifySchema({obj:schemaCopy, decorator: data => {
                // console.log("decoratorDataName", data.name)
                if (data.droppableId == destination.droppableId) {
                    destinationObj = data
                }
                return data
            }})

            const moveResults = add(
                sourceObj.elements,
                destinationObj.elements,
                source,
                destination
            );
            //sourceObj.elements = moveResults[source.droppableId]
            destinationObj.elements = moveResults[destination.droppableId]

            

        } else {
            console.warn("DND not implemented from", from, "to", to)
        }

        setSchema(schemaCopy)


    }

    

    const renderDroppable = (props) => {
        const {elements, elementProps, droppableId} = props
        const {direction} = elementProps || {direction : "column"}

        let droppableDirection = direction == "row" ? "horizontal" : "vertical"

        return (
            <Droppable key={droppableId} droppableId={droppableId} direction={droppableDirection}>
            {(provided, snapshot) => (
            <Grid
                container
                direction={direction}
                
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
            >
                {elements.map((element, index) => (
                <Draggable key={element.name} draggableId={element.draggableId} index={index}>
                    {(provided, snapshot) => (
                    
                    <Grid
                        item
                        
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                        )}
                    >
                        <FormControl fullWidth>

                            {renderElement(element)}

                        </FormControl>
                        
                    
                    </Grid>
                    )}
                </Draggable>
                ))}
                {provided.placeholder}
            </Grid>
            )}
        </Droppable>
        )
    }

    const renderElement = (props) => {
        const {type, name} = props

        if (type == "text") {
            return (
                <>
                <Grid container>
                    <Grid item>
                        DRAG
                    </Grid>
                    <Grid item>
                        <TextField label={name}></TextField>
                    </Grid>
                </Grid>
                    
                </>
            )
        } else return false

    }

    const renderelements = ({elements}) => {
        if (elements) {
            return elements.map( e => {
                if (e.type == "grid") {
                    return renderDroppable(e)
                } else {
                    return renderElement(e)
                }
            })
        }
    }

    if (schema) {
        return (
            <>
            <DragDropContext onDragEnd={onDragEnd}>
                {renderelements(elementsListSchema)}
                {renderelements(schema)}
            </DragDropContext>
            <pre>{JSON.stringify(schema, 0, 2)}</pre>
            </>
        );
    } else return false


}

export default Dnd