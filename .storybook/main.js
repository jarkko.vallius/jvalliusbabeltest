module.exports = {
  stories: ['../stories/**/*.stories.(js|mdx)'],
  addons: [
    '@storybook/addon-actions', 
    '@storybook/addon-links',
    '@storybook/addon-docs'
  ],
};

// stories: ['../src/**/*.stories.(js|mdx)'],
// addons: ['@storybook/addon-docs'],