import babel from 'rollup-plugin-babel';
// import resolve from '@rollup/plugin-node-resolve';
import pkg from './package.json';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';

//       json(),
//       resolve({ extensions }),
//       babel(getBabelOptions({ useESModules: true })),
//       sizeSnapshot(snapshotArgs),
//     ],
//   },

const babelPresets = [
    ["@babel/preset-env", {
        useBuiltIns: 'usage',
        corejs : 3
    }],
    ["@babel/preset-react"],
    //["minify"]
]

const babelPlugins = [
    [
      'import',
      {
        'libraryName': '@material-ui/core',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        'libraryDirectory': 'esm',
        'camel2DashComponentName': false
      },
      'core'
    ],
    [
      'import',
      {
        'libraryName': '@material-ui/icons',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        'libraryDirectory': 'esm',
        'camel2DashComponentName': false
      },
      'icons'
    ]
  ];

const getBabelOptions = () => ({
    presets : babelPresets,
    plugins : babelPlugins,
    //comments: false
})

const config = {
	input: 'src/index.js',
    output: { file: pkg.main, format: 'esm' },
    plugins : [
        babel(getBabelOptions()),
        peerDepsExternal(),
    ],
};

export default config;
